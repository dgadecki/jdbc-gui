

insert into uslugi (nazwa,cena)
values ('Premium', 250.5),
	   ('Business',300.12), 
       ('Economy',50);
 
insert into klasy_samochodow (nazwa, cena_za_dzien)
values ('Miejskie', 50),
	   ('Małe', 70),
       ('Kompaktowe', 90),
       ('Średnia', 130),
       ('Wyższa', 180),
       ('Luksusowa', 250),
       ('Sportowe', 500),
       ('SUV', 200);
       
insert into Stany_samochodu (stan)
values ('Dostepny'),
	   ('Zarezerwowany'),
       ('W naprawie'),
       ('Wypozyczony');
       
insert into wypozyczalnie (miasto, ulica, nr_budynku, nr_lokalu, kod_pocztowy, nr_telefonu)
values ('Wrocław', 'Kiełczowska', '134', '12', '51-180', '354654789'),
       ('Warszawa', 'Wesoła', '102', '15', '05-075', '345498798'),
       ('Krakow', 'Tuchowska', '341', '9', '30-690', '567876567'),
       ('Poznań', 'Łazarska', '98', '23', '61-680', '345498709');
       
 
insert into samochody (nr_rejestracyjny, marka, model_samochodu, rok_produkcji, rodzaj_silnika, moc_silnika, Klasy_samochodow_id_klasy, Wypozyczalnie_id_wypozyczalni, Stany_samochodu_id_stanu)
values ('EPI98SG', 'Ford', 'Mondeo', 2009, 'Diesel', 101, 4, 1, 1),
       ('DWR87HG', 'Toyota', 'Yaris', 2010, 'Diesel', 65, 2, 1, 2),
	   ('EPIR881', 'Mazda', 'CX-7', 2008, 'Benzyna', 230, 8, 1, 3),
       ('DW65789', 'Citroen', 'C1', 2011, 'Benzyna', 45, 1, 1, 4),
       ('DW12345', 'Ford', 'Focus', 2010, 'Diesel', 90, 3, 1, 4),
       ('DW56789', 'Audi', 'A6', 2012, 'Benzyna', 150, 5, 1, 2),
       ('DW98765', 'Volkswagen', 'Phaeton', 2012, 'Benzyna', 200, 6, 1, 1),
       ('DW88899', 'Porsche', '911', 2013, 'Benzyna', 450, 7, 1, 3);
       
insert into klienci (imie, nazwisko, pesel, nr_telefonu, login, haslo)
values ('Jan', 'Kowalski', '93070910345', '987678765', 'jankow', 'a8393058e7f0735a5578a6f288c388dc'),
	   ('Zbigniew', 'Nowak', '93080913298', '987123765', 'zbinow', '160af033fb05136b11ec2f9489704394'),
       ('Anna', 'Lewandowska', '89080913298', '923123765', 'annlew', 'ea57840c00c2c07b493734dc3ec74015'),
       ('Janina', 'Koziwąs', '87100913345', '567123765', 'jankoz', 'c2484bf66c93806ae690b568b6a51741'),
       ('Zygmunt', 'Wiśniewski', '78120913298', '678123765', 'zygwis', '7105778af575fd07a07b88ca345fd03b'),
       ('Remigiusz', 'Wolski', '79061213298', '345987092', 'remwol', '009bab790bf83546a7befb5923cf802a');
       
insert into pracownicy(imie, nazwisko, pesel, e_mail, nr_telefonu,login,haslo)
values ('Bartosz', 'Łągwa', '70090867890', 'bl@carcompany.com', '444555666','barlag', '57e03c0ff06b60fd98a7113c4143b914'),
	   ('Michał', 'Wiśniewski', '89090867890', 'mw@carcompany.com', '777555666', 'micwis', '7105778af575fd07a07b88ca345fd03b'),
       ('Daria', 'Bogdan', '87090867890', 'db@carcompany.com', '444555777', 'darbog', 'dd677267f076e036dd1a3a36949375d3'),
       ('Daria2', 'Bogdan2', '87090867820', 'db2@carcompany.com', '444553777', 'darbog2', '2f6282c140bdacee2dffeb702b767ef9'),
       ('Michał2', 'Wiśniewski2', '89090867390', 'mw2@carcompany.com', '777655666', 'micwis2', 'b9f619dfd65b49413ee02678127a2b57');
       
insert into przeglady(data_przegladu,koniec_przegladu,Samochody_id_samochodu,Pracownicy_id_pracownika)
values ('2015-03-01','2016-03-01', 1, 3), 
	    ('2015-06-01','2016-06-01', 2, 1),
        ('2015-12-01','2016-12-11', 3, 1),
        ('2015-06-01','2016-04-01', 4, 1),
        ('2015-03-01','2016-06-01', 5, 3),
        ('2015-06-01','2016-05-01', 6, 2),
        ('2015-06-01','2016-02-21', 7, 2),
        ('2015-06-01','2016-03-01', 8, 2);
        
         
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-5-1 00:00:00','2015-5-11 00:00:00',120,1,null,3);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-07-19 00:00:00',200,3,2,2);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-05-30 00:00:00',300,3,1,2);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-15 00:00:00','2015-05-29 00:00:00',500,3,5,3);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-06-19 00:00:00',154,4,1,4);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-11 00:00:00','2015-05-23 00:00:00',871,5,1,1);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-02-07 00:00:00','2015-05-19 00:00:00',322,5,5,1);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-01-07 00:00:00','2015-05-19 00:00:00',433,1,5,3);

INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-01 00:00:00','2015-05-11 00:00:00',133,6,3,3);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-07-19 00:00:00',111,7,2,2);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-05-30 00:00:00',111,8,1,2);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-15 00:00:00','2015-05-29 00:00:00',222,8,3,3);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-06-19 00:00:00',333,7,1,4);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-11 00:00:00','2015-05-23 00:00:00',321,6,3,1);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-02-07 00:00:00','2015-05-19 00:00:00',123,5,3,1);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-01-07 00:00:00','2015-05-19 00:00:00',321,1,1,3);

INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-01 00:00:00','2015-05-11 00:00:00',333,8,2,3);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-07-19 00:00:00',321,3,4,2);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-05-30 00:00:00',111,3,4,2);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-15 00:00:00','2015-05-29 00:00:00',321,3,2,3);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-06-19 00:00:00',111,4,1,4);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-11 00:00:00','2015-05-23 00:00:00',321,6,4,1);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-02-07 00:00:00','2015-05-19 00:00:00',111,5,4,1);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-01-07 00:00:00','2015-05-19 00:00:00',222,7,4,3);

INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-01 00:00:00','2015-05-11 00:00:00',321,7,5,3);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-07-19 00:00:00',222,7,2,2);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-05-30 00:00:00',111,8,1,2);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-15 00:00:00','2015-05-29 00:00:00',222,3,5,3);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-06-19 00:00:00',333,4,1,4);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-11 00:00:00','2015-05-23 00:00:00',111,8,5,1);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-02-07 00:00:00','2015-05-19 00:00:00',111,5,2,1);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-01-07 00:00:00','2015-05-19 00:00:00',321,1,1,3);

INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-01 00:00:00','2015-05-11 00:00:00',211,1,4,3);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-07-19 00:00:00',333,7,4,2);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-05-30 00:00:00',222,3,4,2);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-15 00:00:00','2015-05-29 00:00:00',111,7,4,3);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-07 00:00:00','2015-06-19 00:00:00',757,4,1,4);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-05-11 00:00:00','2015-05-23 00:00:00',111,6,3,1);
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-02-07 00:00:00','2015-05-19 00:00:00',214,3,3,'1');
INSERT INTO `wypozyczenia` (`poczatek`,`koniec`,`oplata_koncowa`,`Samochody_id_samochodu`,`Pracownicy_id_pracownika`,`Klienci_id_klienta`) VALUES ('2015-01-07 00:00:00','2015-05-19 00:00:00',199,1,3,3);

        
insert into lista_uslug(Uslugi_id_uslugi,Wypozyczenia_id_wypozyczenia)
values (1,2),
		(2,1),
        (1,1);
        
insert into ubezpieczenia(poczatek,koniec,nazwa_ubezpieczyciela, Samochody_id_samochodu)
values ('2015-08-01','2016-08-01','PZU',1),
         ('2015-08-11','2015-12-11','Aviva',2),
          ('2015-08-11','2015-09-11','Benk',3),
           ('2015-08-11','2016-08-11','PZU',4),
            ('2015-05-11','2016-08-11','Aviva',5),
             ('2015-08-11','2016-08-11','Aviva',6)
            , ('2015-01-11','2015-03-11','PZU',7)
            , ('2015-08-11','2016-08-11','Aviva',8);
         

         

       
       