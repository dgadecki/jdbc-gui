drop database if exists bazy;
create database bazy;
use bazy;
SET SQL_SAFE_UPDATES = 0;


CREATE TABLE IF NOT EXISTS Klasy_samochodow
  (
    id_klasy      INT NOT NULL AUTO_INCREMENT,
    nazwa         VARCHAR (20) NOT NULL ,
    cena_za_dzien FLOAT NOT NULL,
    PRIMARY KEY (id_klasy)
  ) ;
  
  CREATE TABLE IF NOT EXISTS Stany_samochodu
  (
  id_stanu INT NOT NULL AUTO_INCREMENT,
  stan 		VARCHAR(20) NOT NULL,
  PRIMARY KEY (id_stanu)
  );
  
CREATE TABLE IF NOT EXISTS Klienci
  (
    id_klienta  INT NOT NULL AUTO_INCREMENT,
    imie        VARCHAR (20) NOT NULL ,
    drugie_imie VARCHAR (20) ,
    nazwisko    VARCHAR (30) NOT NULL ,
    pesel       VARCHAR (11) ,
    nr_dowodu   VARCHAR (9) ,
    e_mail      VARCHAR (25) ,
    nr_telefonu VARCHAR (12) NOT NULL,
    login		VARCHAR (20) NOT NULL,
    haslo		VARCHAR (60) NOT NULL,
        PRIMARY KEY (id_klienta)
  ) ;

CREATE TABLE IF NOT EXISTS Lista_uslug
  (
    Uslugi_id_uslugi             INT NOT NULL,
    Wypozyczenia_id_wypozyczenia INT NOT NULL
  ) ;

CREATE TABLE IF NOT EXISTS Pracownicy
  (
    id_pracownika INT NOT NULL AUTO_INCREMENT,
    imie          VARCHAR (20) NOT NULL ,
    drugie_imie   VARCHAR (20) ,
    nazwisko      VARCHAR (30) NOT NULL ,
    pesel         VARCHAR (11) ,
    nr_dowodu     VARCHAR (9) ,
    e_mail        VARCHAR (25) NOT NULL ,
    nr_telefonu   VARCHAR (12) NOT NULL ,
    stanowisko    VARCHAR (30),
    login		  VARCHAR (20) NOT NULL,
    haslo		  VARCHAR (60) NOT NULL,
        PRIMARY KEY (id_pracownika)
  ) ;


CREATE TABLE IF NOT EXISTS Przeglady
  (
    data_przegladu           DATE NOT NULL ,
    koniec_przegladu         DATE NOT NULL ,
    miejsce_przegladu        VARCHAR (20) ,
    Samochody_id_samochodu   INT NOT NULL ,
    Pracownicy_id_pracownika INT NOT NULL ,
    miasto                   VARCHAR (20) ,
    ulica                    VARCHAR (20) ,
    nr_budynku               VARCHAR (5) ,
    nr_lokalu                VARCHAR (5) ,
    kod_pocztowy             VARCHAR (5) ,
	PRIMARY KEY (Samochody_id_samochodu)
  ) ;


CREATE TABLE IF NOT EXISTS Samochody
  (
    id_samochodu                  INT NOT NULL AUTO_INCREMENT,
    nr_rejestracyjny              VARCHAR (7) NOT NULL ,
    marka                         VARCHAR (15) NOT NULL ,
    model_samochodu               VARCHAR (15) NOT NULL ,
    rok_produkcji                 YEAR ,
    rodzaj_silnika                VARCHAR (10) ,
    moc_silnika                   INT ,
    Klasy_samochodow_id_klasy     INT NOT NULL ,
    Wypozyczalnie_id_wypozyczalni INT NOT NULL,
    Stany_Samochodu_id_stanu      INT NOT NULL,
	PRIMARY KEY (id_samochodu)
  ) ;

CREATE TABLE IF NOT EXISTS Ubezpieczenia
  (
    poczatek                    DATE NOT NULL ,
    koniec                      DATE NOT NULL ,
    nazwa_ubezpieczyciela       VARCHAR (40) NOT NULL ,
    nr_telefonu_ubezpieczyciela VARCHAR (12) ,
    Samochody_id_samochodu      INT NOT NULL,
    primary key (Samochody_id_samochodu)
  ) ;

CREATE TABLE IF NOT EXISTS Uslugi
  (
    id_uslugi INT NOT NULL AUTO_INCREMENT,
    nazwa     VARCHAR (30) NOT NULL ,
    cena      FLOAT NOT NULL,
    primary key (id_uslugi)
  ) ;

CREATE TABLE IF NOT EXISTS Wypozyczalnie
  (
    id_wypozyczalni INT NOT NULL AUTO_INCREMENT,
    miasto          VARCHAR (20) NOT NULL ,
    ulica           VARCHAR (20) NOT NULL ,
    nr_budynku      VARCHAR (5) NOT NULL ,
    nr_lokalu       VARCHAR (4) ,
    kod_pocztowy    VARCHAR (6) NOT NULL ,
    nr_telefonu     VARCHAR (12) NOT NULL,
    primary key (id_wypozyczalni)
  ) ;

CREATE TABLE IF NOT EXISTS Wypozyczenia
  (
    id_wypozyczenia          INT NOT NULL AUTO_INCREMENT,
    poczatek                 DATETIME NOT NULL ,
    koniec                   DATETIME NOT NULL ,
    oplata_koncowa           FLOAT ,
    Samochody_id_samochodu   INT NOT NULL ,
    Pracownicy_id_pracownika INT ,
    Klienci_id_klienta       INT NOT NULL,
    primary key (id_wypozyczenia)
  ) ;

ALTER TABLE Lista_uslug ADD CONSTRAINT Lista_uslug_Uslugi_FK FOREIGN KEY ( Uslugi_id_uslugi ) REFERENCES Uslugi ( id_uslugi ) ;

ALTER TABLE Lista_uslug ADD CONSTRAINT Lista_uslug_Wypozyczenia_FK FOREIGN KEY ( Wypozyczenia_id_wypozyczenia ) REFERENCES Wypozyczenia ( id_wypozyczenia ) ;

ALTER TABLE Przeglady ADD CONSTRAINT Przeglady_Pracownicy_FK FOREIGN KEY ( Pracownicy_id_pracownika ) REFERENCES Pracownicy ( id_pracownika ) ;

ALTER TABLE Przeglady ADD CONSTRAINT Przeglady_Samochody_FK FOREIGN KEY ( Samochody_id_samochodu ) REFERENCES Samochody ( id_samochodu ) ;

ALTER TABLE Samochody ADD CONSTRAINT Samochody_Klasy_samochodow_FK FOREIGN KEY ( Klasy_samochodow_id_klasy ) REFERENCES Klasy_samochodow ( id_klasy ) ;

ALTER TABLE Samochody ADD CONSTRAINT Samochody_Wypozyczalnie_FK FOREIGN KEY ( Wypozyczalnie_id_wypozyczalni ) REFERENCES Wypozyczalnie ( id_wypozyczalni ) ;

ALTER TABLE Samochody ADD CONSTRAINT Samochody_Stany_Samochodu_FK FOREIGN KEY ( Stany_samochodu_id_stanu ) REFERENCES Stany_Samochodu ( id_stanu ) ;

ALTER TABLE Ubezpieczenia ADD CONSTRAINT Ubezpieczenia_Samochody_FK FOREIGN KEY ( Samochody_id_samochodu ) REFERENCES Samochody ( id_samochodu ) ;

ALTER TABLE Wypozyczenia ADD CONSTRAINT Wypozyczenia_Klienci_FK FOREIGN KEY ( Klienci_id_klienta ) REFERENCES Klienci ( id_klienta ) ;

ALTER TABLE Wypozyczenia ADD CONSTRAINT Wypozyczenia_Pracownicy_FK FOREIGN KEY ( Pracownicy_id_pracownika ) REFERENCES Pracownicy ( id_pracownika ) ;

ALTER TABLE Wypozyczenia ADD CONSTRAINT Wypozyczenia_Samochody_FK FOREIGN KEY ( Samochody_id_samochodu ) REFERENCES Samochody ( id_samochodu ) ;


