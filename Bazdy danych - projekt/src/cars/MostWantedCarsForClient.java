package cars;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

public class MostWantedCarsForClient extends JFrame {

    private JPanel contentPane;
    private JTable table;
    private JSpinner spinnerNCars;
    private SamochodyDAO samochodyDAO;

    /**
     * Launch the application.
     */
    // public static void main(String[] args) {
    // EventQueue.invokeLater(new Runnable() {
    // public void run() {
    // try {
    // MostWantedCars frame = new MostWantedCars();
    // frame.setVisible(true);
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // }
    // });
    // }

    /**
     * Create the frame.
     */

    private void hideColumns(int... index) {
        for (int i : index) {
            table.getColumnModel().getColumn(i).setMinWidth(0);
            table.getColumnModel().getColumn(i).setMaxWidth(0);
        }
    }

    public MostWantedCarsForClient() {
        try {
            samochodyDAO = new SamochodyDAO();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        FlowLayout flowLayout = (FlowLayout) panel.getLayout();
        flowLayout.setAlignOnBaseline(true);
        flowLayout.setAlignment(FlowLayout.LEFT);
        contentPane.add(panel, BorderLayout.NORTH);

        JLabel lblNumberOfCars = new JLabel("Liczba samochodów:");
        panel.add(lblNumberOfCars);

        spinnerNCars = new JSpinner();
        spinnerNCars.setModel(new SpinnerNumberModel(new Integer(1),
                new Integer(1), null, new Integer(1)));
        panel.add(spinnerNCars);

        JButton btnSearch = new JButton("Search");
        btnSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                List<Samochod> cars;
                List<Samochod> allCars;
                try {
                    int nCars = (int) spinnerNCars.getValue();
                    cars = samochodyDAO.getMostOftenRentedCars(nCars, false);
                    SamochodyTableModel samochodyTableModel = new SamochodyTableModel(
                            cars);
                    table.setModel(samochodyTableModel);
                    hideColumns(3);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(MostWantedCarsForClient.this,
                            "Error: " + e, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        panel.add(btnSearch);

        JScrollPane scrollPane = new JScrollPane();
        contentPane.add(scrollPane, BorderLayout.CENTER);

        table = new JTable();
        scrollPane.setViewportView(table);
    }

    public JPanel getJPanel() {
        return contentPane;
    }
}
