package cars;

import klasa.KlasaSamochodu;
import stan.StanSamochodu;
import wypozyczalnia.Wypozyczalnia;

public class Samochod {
    private int id = -1;
    private String marka = "";
    private String model = "";
    private String nrRejestracyjny = "";
    private int mocSilnika;
    private int iloscWystapien = 0;

    private int rokProdukcji;
    private String rodzajSilnika = "";
    private int idStanu;
    private int idWypozyczalni;
    private int idKlasy;
    private String stan = "";
    private String wypozyczalnia = "";
    private String klasa = "";
    private KlasaSamochodu klasaSamochoduObj;
    private StanSamochodu stanSamochoduObj;
    private Wypozyczalnia wypozyczalniaObj;

    public Samochod(int id, String marka, String model, String nrRejestracyjny,
            int mocSilnika) {
        this.id = id;
        this.marka = marka;
        this.model = model;
        this.nrRejestracyjny = nrRejestracyjny;
        this.mocSilnika = mocSilnika;
    }
    
    public Samochod(int id, String marka, String model, String nrRejestracyjny){
    	this.id = id;
    	this.marka = marka;
    	this.model = model;
    	this.nrRejestracyjny = nrRejestracyjny;
    }

    public Samochod(int id, String marka, String model, String nrRejestracyjny,
            int mocSilnika, int rokProdukcji, String rodzajSilnika,
            String stan, String wypozyczalnia, String klasa, int idStanu,
            int idWypozyczalni, int idKlasy) {
        this.id = id;
        this.marka = marka;
        this.model = model;
        this.nrRejestracyjny = nrRejestracyjny;
        this.mocSilnika = mocSilnika;
        this.rokProdukcji = rokProdukcji;
        this.rodzajSilnika = rodzajSilnika;
        this.stan = stan;
        this.wypozyczalnia = wypozyczalnia;
        this.klasa = klasa;
        this.idStanu = idStanu;
        this.idWypozyczalni = idWypozyczalni;
        this.idKlasy = idKlasy;
        klasaSamochoduObj = new KlasaSamochodu(idKlasy, klasa, 0);
        stanSamochoduObj = new StanSamochodu(idStanu, stan);
        wypozyczalniaObj = new Wypozyczalnia(idWypozyczalni, wypozyczalnia);
    }

    public KlasaSamochodu getKlasaSamochoduObj() {
        return klasaSamochoduObj;
    }

    public StanSamochodu getStanSamochoduObj() {
        return stanSamochoduObj;
    }

    public Wypozyczalnia getWypozyczalniaObj() {
        return wypozyczalniaObj;
    }

    public int getId() {
        return id;
    }

    public int getmocSilnika() {
        return mocSilnika;
    }

    public String getmarka() {
        return marka;

    }

    public String getmodel() {
        return model;
    }

    public String getnrRejestracyjny() {
        return nrRejestracyjny;
    }

    public int getiloscWystapien() {
        return iloscWystapien;
    }

    public int getrokProdukcji() {
        return rokProdukcji;
    }

    public String getrodzajSilnika() {
        return rodzajSilnika;
    }

    public int getidStanu() {
        return idStanu;
    }

    public int getidWypozyczalni() {
        return idWypozyczalni;
    }

    public int getidKlasy() {
        return idKlasy;
    }

    public String getStan() {
        return stan;
    }

    public String getWypozyczalnia() {
        return wypozyczalnia;
    }

    public String getKlasa() {
        return klasa;
    }

    public void setIloscWystapien(int ilosc) {
        iloscWystapien = ilosc;
    }

}
