package cars;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import stan.StanSamochodu;
import stan.StanSamochoduDAO;
import all.AddNewCarPanel;

public class FindCarPanel extends JPanel {
    private JTextField textFieldMarka;
    private JTextField textFieldModel;
    JComboBox<StanSamochodu> cbStanSamochodu = new JComboBox<>();
    JSpinner spinnerRokProdukcji = new JSpinner();
    JRadioButton rdbtnRokProdukcji = new JRadioButton("Rok produkcji");
    JLabel lblMarka = new JLabel("Marka");
    JLabel lblModel = new JLabel("Model");
    private JTable table;
    JButton btnSzukaj = new JButton("Szukaj");
    SamochodyDAO samochodyDAO;
    private final JRadioButton rdbtnStan = new JRadioButton("Stan");
    JScrollPane scrollPane = new JScrollPane();
    List<Samochod> cars;

    private void hideColumns(int... index) {
        for (int i : index) {
            table.getColumnModel().getColumn(i).setMinWidth(0);
            table.getColumnModel().getColumn(i).setMaxWidth(0);
        }
    }

    /**
     * Create the panel.
     */
    public FindCarPanel() {
        try {
            samochodyDAO = new SamochodyDAO();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        setLayout(null);

        textFieldMarka = new JTextField();
        textFieldMarka.setBounds(30, 35, 117, 20);
        add(textFieldMarka);
        textFieldMarka.setColumns(10);

        textFieldModel = new JTextField();
        textFieldModel.setColumns(10);
        textFieldModel.setBounds(174, 35, 117, 20);
        add(textFieldModel);
        spinnerRokProdukcji.setModel(new SpinnerNumberModel(new Integer(2014),
                new Integer(0), null, new Integer(1)));

        spinnerRokProdukcji.setBounds(334, 35, 83, 20);
        spinnerRokProdukcji.setEnabled(false);
        add(spinnerRokProdukcji);

        cbStanSamochodu.setBounds(446, 35, 117, 20);
        List<StanSamochodu> st = new ArrayList<StanSamochodu>();
        try {
            st = new StanSamochoduDAO().getAllStanSamochodu();
        } catch (Exception e) {

        }
        for (StanSamochodu s : st) {
            cbStanSamochodu.addItem(s);
        }
        cbStanSamochodu.setEnabled(false);
        add(cbStanSamochodu);
        rdbtnRokProdukcji.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                spinnerRokProdukcji.setEnabled(rdbtnRokProdukcji.isSelected());
            }
        });

        rdbtnRokProdukcji.setBounds(334, 7, 109, 23);
        add(rdbtnRokProdukcji);

        lblMarka.setBounds(31, 11, 69, 14);
        add(lblMarka);

        lblModel.setBounds(174, 10, 46, 14);
        add(lblModel);

        scrollPane.setBounds(10, 80, 952, 334);
        add(scrollPane);

        table = new JTable();
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                JTable tbl = (JTable) arg0.getSource();

                if (arg0.getClickCount() >= 2) {
                    JFrame jFrame = new JFrame();
                    AddNewCarPanel addNewCarPanel = new AddNewCarPanel();
                    addNewCarPanel.btnUpdate.setVisible(true);
                    addNewCarPanel.btnDodaj.setVisible(false);
                    Point p = arg0.getPoint();
                    int row = tbl.rowAtPoint(p);
                    addNewCarPanel.insertData(cars.get(row));
                    addNewCarPanel.actualCar = cars.get(row);
                    jFrame.add(addNewCarPanel);
                    jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    jFrame.setSize(350, 370);
                    jFrame.setVisible(true);
                }
            }
        });
        scrollPane.setViewportView(table);
        btnSzukaj.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                try {
                    int rokProdukcji;
                    int idStanu;
                    if (!rdbtnRokProdukcji.isSelected()) {
                        rokProdukcji = 0;
                    }
                    else {
                        rokProdukcji = (int) spinnerRokProdukcji.getValue();
                    }
                    if (!rdbtnStan.isSelected()) {
                        idStanu = -1;
                    }
                    else {
                        idStanu = ((StanSamochodu) cbStanSamochodu
                                .getSelectedItem()).getId();
                    }
                    cars = samochodyDAO.searchCar(textFieldMarka.getText(),
                            textFieldModel.getText(), rokProdukcji, idStanu);
                    SamochodyTableModel samochodyTableModel = new SamochodyTableModel(
                            cars);
                    table.setModel(samochodyTableModel);
                    hideColumns(0);

                } catch (Exception e) {
                    JOptionPane.showMessageDialog(FindCarPanel.this, "Error: "
                            + e, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        btnSzukaj.setBounds(603, 34, 89, 23);
        add(btnSzukaj);
        rdbtnStan.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cbStanSamochodu.setEnabled(rdbtnStan.isSelected());

            }
        });

        rdbtnStan.setBounds(454, 7, 109, 23);

        add(rdbtnStan);

    }
}
