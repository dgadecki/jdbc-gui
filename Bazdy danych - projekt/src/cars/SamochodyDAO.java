package cars;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class SamochodyDAO {
    private Connection myConn;

    public SamochodyDAO() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileInputStream("bazydanych.properties"));

        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String dburl = properties.getProperty("dburl");

        myConn = DriverManager.getConnection(dburl, user, password);
    }

    public List<Samochod> getAllSamochody() throws Exception {
        List<Samochod> cars = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt
                    .executeQuery("select T2.*, T3.stan, T4.nazwa, T5.miasto from samochody T2"
                            + " inner join stany_samochodu T3 on T2.Stany_Samochodu_id_stanu = T3.id_stanu "
                            + " inner join klasy_samochodow T4 on T4.id_klasy = T2.Klasy_samochodow_id_klasy "
                            + " inner join wypozyczalnie T5 on T2.Wypozyczalnie_id_wypozyczalni = T5.id_wypozyczalni");
            while (myRs.next()) {
                Samochod samochod = convertRowToSamochody(myRs);
                cars.add(samochod);
            }
            return cars;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    public List<Samochod> getAllDostepneSamochody() throws Exception {
        List<Samochod> cars = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt
                    .executeQuery("select T2.*, T3.stan, T4.nazwa, T5.miasto from samochody T2"
                            + " inner join stany_samochodu T3 on T2.Stany_Samochodu_id_stanu = T3.id_stanu "
                            + " inner join klasy_samochodow T4 on T4.id_klasy = T2.Klasy_samochodow_id_klasy "
                            + " inner join wypozyczalnie T5 on T2.Wypozyczalnie_id_wypozyczalni = T5.id_wypozyczalni"
                            + " where T2.Stany_Samochodu_id_stanu = 1");
            while (myRs.next()) {
                Samochod samochod = convertRowToSamochody(myRs);
                cars.add(samochod);
            }
            return cars;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    public List<Samochod> getMostOftenRentedCars(int numberOfCars,
            boolean sortAscend) throws Exception {
        String typeOfSorting = "ASC";
        Statement myStmt = null;
        ResultSet myRs = null;
        List<Samochod> cars = new ArrayList<>();
        if (!sortAscend) {
            typeOfSorting = "DESC";
        }
        String query = "select count(T1.Samochody_id_samochodu) as "
                + "ilosc,T2.*, T3.stan, T4.nazwa,"
                + "T5.miasto from wypozyczenia T1 "
                + "right join samochody T2 " + "on T2.id_samochodu = "
                + "T1.Samochody_id_samochodu inner join "
                + "stany_samochodu T3 on "
                + "T2.Stany_Samochodu_id_stanu = T3.id_stanu "
                + "inner join klasy_samochodow T4 on T4.id_klasy ="
                + " T2.Klasy_samochodow_id_klasy "
                + "inner join wypozyczalnie T5 on "
                + "T2.Wypozyczalnie_id_wypozyczalni = T5.id_wypozyczalni "
                + " group by T1.Samochody_id_samochodu order by ilosc "
                + typeOfSorting + " limit " + numberOfCars;
        try {
            myStmt = myConn.createStatement();
            myRs = myStmt.executeQuery(query);
            while (myRs.next()) {
                Samochod samochod = convertRowToSamochody(myRs);
                samochod.setIloscWystapien(myRs.getInt("ilosc"));
                cars.add(samochod);
            }
            return cars;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    private Samochod convertRowToSamochody(ResultSet myRs) throws SQLException {
        Samochod samochod = new Samochod(myRs.getInt("id_samochodu"),
                myRs.getString("marka"), myRs.getString("model_samochodu"),
                myRs.getString("nr_rejestracyjny"), myRs.getInt("moc_silnika"),
                myRs.getInt("rok_produkcji"), myRs.getString("rodzaj_silnika"),
                myRs.getString("stan"), myRs.getString("miasto"),
                myRs.getString("nazwa"),
                myRs.getInt("Stany_Samochodu_id_stanu"),
                myRs.getInt("Wypozyczalnie_id_wypozyczalni"),
                myRs.getInt("Klasy_samochodow_id_klasy"));
        return samochod;
    }

    public List<Samochod> searchCar(String marka, String model,
            int rokProdukcji, int idStanu) throws Exception {
        List<Samochod> cars = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        marka = "'%" + marka + "%'";
        model = "'%" + model + "%'";
        String stan = "";
        if (idStanu >= 0) {
            stan = " AND T2.Stany_Samochodu_id_stanu=" + idStanu;
        }

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt
                    .executeQuery("select T2.*, T3.stan, T4.nazwa, T5.miasto from samochody T2"
                            + " inner join stany_samochodu T3 on T2.Stany_Samochodu_id_stanu = T3.id_stanu "
                            + "  inner join klasy_samochodow T4 on T4.id_klasy = T2.Klasy_samochodow_id_klasy "
                            + "  inner join wypozyczalnie T5 on T2.Wypozyczalnie_id_wypozyczalni = T5.id_wypozyczalni "
                            + "  where T2.marka like"
                            + marka
                            + " AND T2.model_samochodu like"
                            + model
                            + " AND T2.rok_produkcji>" + rokProdukcji + stan);

            while (myRs.next()) {
                Samochod samochod = convertRowToSamochody(myRs);
                cars.add(samochod);

            }
            return cars;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    public List<Samochod> searchCarFoClient(String marka, String model,
            int rokProdukcji, int idKlasy) throws Exception {
        List<Samochod> cars = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        marka = "'%" + marka + "%'";
        model = "'%" + model + "%'";
        String stan = "";
        if (idKlasy >= 0) {
            stan = " AND T2.Klasy_samochodow_id_klasy=" + idKlasy;
        }

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt
                    .executeQuery("select T2.*, T3.stan, T4.nazwa, T5.miasto from samochody T2"
                            + " inner join stany_samochodu T3 on T2.Stany_Samochodu_id_stanu = T3.id_stanu "
                            + "  inner join klasy_samochodow T4 on T4.id_klasy = T2.Klasy_samochodow_id_klasy "
                            + "  inner join wypozyczalnie T5 on T2.Wypozyczalnie_id_wypozyczalni = T5.id_wypozyczalni "
                            + "  where T2.marka like"
                            + marka
                            + " AND T2.model_samochodu like"
                            + model
                            + " AND T2.rok_produkcji>"
                            + rokProdukcji
                            + stan
                            + " AND T2.Stany_Samochodu_id_stanu=1 ");

            while (myRs.next()) {
                Samochod samochod = convertRowToSamochody(myRs);
                cars.add(samochod);

            }
            return cars;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    public void updateSamochod(Samochod car) throws Exception {
        Statement myStmt = null;

        try {
            myStmt = myConn.createStatement();
            myStmt.executeUpdate("update samochody "
                    + " set nr_rejestracyjny='" + car.getnrRejestracyjny()
                    + "', marka='" + car.getmarka() + "', model_samochodu='"
                    + car.getmodel() + "', rok_produkcji="
                    + car.getrokProdukcji() + ", rodzaj_silnika='"
                    + car.getrodzajSilnika() + "', moc_silnika="
                    + car.getmocSilnika() + ", Klasy_samochodow_id_klasy="
                    + car.getKlasaSamochoduObj().getId()
                    + ", Wypozyczalnie_id_wypozyczalni="
                    + car.getWypozyczalniaObj().getId()
                    + ", Stany_Samochodu_id_stanu="
                    + car.getStanSamochoduObj().getId()
                    + " where id_samochodu=" + car.getId());
        } finally {
            myStmt.close();
        }
    }

    public void addSamochod(Samochod car) throws Exception {
        Statement myStmt = null;

        try {
            myStmt = myConn.createStatement();
            myStmt.executeUpdate("insert into samochody (nr_rejestracyjny, marka, model_samochodu, "
                    + " rok_produkcji, rodzaj_silnika, moc_silnika, Klasy_samochodow_id_klasy, "
                    + " Wypozyczalnie_id_wypozyczalni, Stany_samochodu_id_stanu) values ('"
                    + car.getnrRejestracyjny()
                    + "', '"
                    + car.getmarka()
                    + "', '"
                    + car.getmodel()
                    + "', "
                    + car.getrokProdukcji()
                    + ", '"
                    + car.getrodzajSilnika()
                    + "', "
                    + car.getmocSilnika()
                    + ", "
                    + car.getidKlasy()
                    + ", "
                    + car.getidWypozyczalni() + ", " + car.getidStanu() + ")");
        } finally {
            myStmt.close();
        }
    }

}
