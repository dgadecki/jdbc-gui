package cars;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import klient.KlientDAO;
import all.Login;
import all.Miesiace;

public class RentCarPanel extends JPanel {
    private JLabel lblMarka = new JLabel("Marka");
    private JLabel lblModel = new JLabel("Model");
    private JLabel lblKlasa = new JLabel("Klasa");
    private JLabel lblWypoyczalnia = new JLabel("Wypo\u017Cyczalnia");
    private JLabel lblMoc = new JLabel("Moc");
    private JLabel lblRokProdukcji = new JLabel("Rok produkcji");
    private JLabel lblRodzajSilnika = new JLabel("Rodzaj silnika");
    private final JTextField txtMarka = new JTextField();
    private final JTextField txtModel = new JTextField();
    private final JTextField txtRok = new JTextField();
    private final JTextField txtWypozyczalnia = new JTextField();
    private final JTextField txtRodzajsilnika = new JTextField();
    private final JTextField txtMoc = new JTextField();
    private final JTextField txtKlasa = new JTextField();
    private final JButton btnWypozycz = new JButton("Wypozycz");
    private final JLabel lblWypozyczOd = new JLabel("Wypozycz od:");
    private final JSpinner spinnerDzien = new JSpinner();
    private final JComboBox<Miesiace> comboBoxMiesiac = new JComboBox();
    private final JSpinner spinnerRok = new JSpinner();
    private final JSpinner spinnerDzienDo = new JSpinner();
    private final JComboBox comboBoxMiesiacDo = new JComboBox();
    private final JSpinner spinnerRokDo = new JSpinner();
    private List<Miesiace> miesiace = Arrays.asList(Miesiace.values());
    public Samochod actualCar;
    private final JLabel lblWypozyczDo = new JLabel("Wypozycz do:");

    /**
     * Create the panel.
     */
    public RentCarPanel() {
        txtKlasa.setEditable(false);
        txtKlasa.setText("Klasa");
        txtKlasa.setBounds(178, 156, 86, 20);
        txtKlasa.setColumns(10);
        txtMoc.setEditable(false);
        txtMoc.setText("Moc");
        txtMoc.setBounds(178, 131, 86, 20);
        txtMoc.setColumns(10);
        txtRodzajsilnika.setEditable(false);
        txtRodzajsilnika.setText("RodzajSilnika");
        txtRodzajsilnika.setBounds(178, 106, 86, 20);
        txtRodzajsilnika.setColumns(10);
        txtWypozyczalnia.setEditable(false);
        txtWypozyczalnia.setText("Wypozyczalnia");
        txtWypozyczalnia.setBounds(178, 81, 86, 20);
        txtWypozyczalnia.setColumns(10);
        txtRok.setEditable(false);
        txtRok.setText("Rok");
        txtRok.setBounds(178, 56, 86, 20);
        txtRok.setColumns(10);
        txtModel.setEditable(false);
        txtModel.setText("Model");
        txtModel.setBounds(178, 33, 86, 20);
        txtModel.setColumns(10);
        txtMarka.setEditable(false);
        txtMarka.setText("Marka");
        txtMarka.setBounds(178, 8, 86, 20);
        txtMarka.setColumns(10);
        setLayout(null);
        lblMarka.setBounds(40, 11, 46, 14);
        add(lblMarka);

        lblModel.setBounds(40, 36, 46, 14);
        add(lblModel);

        lblRokProdukcji.setBounds(40, 59, 81, 14);
        add(lblRokProdukcji);

        lblRodzajSilnika.setBounds(40, 109, 81, 14);
        add(lblRodzajSilnika);

        lblMoc.setBounds(40, 134, 46, 14);
        add(lblMoc);

        lblKlasa.setBounds(40, 159, 46, 14);
        add(lblKlasa);

        lblWypoyczalnia.setBounds(40, 84, 101, 14);
        add(lblWypoyczalnia);

        add(txtMarka);

        add(txtModel);

        add(txtRok);

        add(txtWypozyczalnia);

        add(txtRodzajsilnika);

        add(txtMoc);

        add(txtKlasa);
        btnWypozycz.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((int) spinnerRok.getValue() > (int) spinnerRokDo.getValue()) {
                    JOptionPane.showMessageDialog(RentCarPanel.this,
                            "Niepoprawna data!");
                    return;
                }
                else if (((Miesiace) comboBoxMiesiac.getSelectedItem()).id > ((Miesiace) comboBoxMiesiacDo
                        .getSelectedItem()).id
                        && (int) spinnerRok.getValue() == (int) spinnerRokDo
                                .getValue()) {
                    JOptionPane.showMessageDialog(RentCarPanel.this,
                            "Niepoprawna data!");
                    return;
                }
                else if (((Miesiace) comboBoxMiesiac.getSelectedItem()).id == ((Miesiace) comboBoxMiesiacDo
                        .getSelectedItem()).id
                        && (int) spinnerRok.getValue() == (int) spinnerRokDo
                                .getValue()
                        && (int) spinnerDzien.getValue() > (int) spinnerDzienDo
                                .getValue()) {
                    JOptionPane.showMessageDialog(RentCarPanel.this,
                            "Niepoprawna data!");
                    return;
                }
                int decide = JOptionPane.showConfirmDialog(RentCarPanel.this,
                        "Czy na pewno chcesz wypozyczyc ten samochod?");
                if (decide == 0) {
                    addWypozyczenie();
                    JOptionPane.showMessageDialog(RentCarPanel.this,
                            "Wypozyczono");
                }
            }
        });
        btnWypozycz.setBounds(88, 187, 134, 43);

        add(btnWypozycz);
        lblWypozyczOd.setBounds(351, 11, 162, 14);

        add(lblWypozyczOd);
        spinnerDzien.setModel(new SpinnerNumberModel(1, 1, 31, 1));
        spinnerDzien.setBounds(351, 33, 46, 20);

        add(spinnerDzien);
        comboBoxMiesiac.setBounds(407, 33, 93, 20);
        for (Miesiace m : miesiace) {
            comboBoxMiesiac.addItem(m);
        }

        add(comboBoxMiesiac);
        spinnerRok.setModel(new SpinnerNumberModel(new Integer(2015),
                new Integer(2015), null, new Integer(1)));
        spinnerRok.setBounds(510, 33, 69, 20);

        add(spinnerRok);
        spinnerDzienDo.setModel(new SpinnerNumberModel(1, 1, 31, 1));
        spinnerDzienDo.setBounds(351, 106, 46, 20);

        add(spinnerDzienDo);
        comboBoxMiesiacDo.setBounds(407, 106, 93, 20);
        for (Miesiace m : miesiace) {
            comboBoxMiesiacDo.addItem(m);
        }

        add(comboBoxMiesiacDo);
        spinnerRokDo.setModel(new SpinnerNumberModel(new Integer(2015),
                new Integer(2015), null, new Integer(1)));
        spinnerRokDo.setBounds(510, 106, 69, 20);

        add(spinnerRokDo);
        lblWypozyczDo.setBounds(351, 84, 162, 14);

        add(lblWypozyczDo);
    }

    public void insertData(Samochod car) {
        txtMarka.setText(car.getmarka());
        txtModel.setText(car.getmodel());
        txtRok.setText(car.getrokProdukcji() + "");
        txtRodzajsilnika.setText(car.getrodzajSilnika());
        txtMoc.setText(car.getmocSilnika() + "");
        txtKlasa.setText(car.getKlasaSamochoduObj().toString());
        txtWypozyczalnia.setText(car.getWypozyczalniaObj().toString());
    }

    public void addWypozyczenie() {
        String since = "'" + ((int) spinnerRok.getValue()) + "-"
                + ((Miesiace) comboBoxMiesiac.getSelectedItem()).getOrder()
                + "-" + ((int) spinnerDzien.getValue()) + " 00:00:00'";
        String to = "'" + ((int) spinnerRokDo.getValue()) + "-"
                + ((Miesiace) comboBoxMiesiacDo.getSelectedItem()).getOrder()
                + "-" + ((int) spinnerDzienDo.getValue()) + " 00:00:00'";

        try {
            KlientDAO klientDAO = new KlientDAO();
            klientDAO.addWypozyczenie(actualCar.getId(), since, to,
                    Login.idKlienta);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
