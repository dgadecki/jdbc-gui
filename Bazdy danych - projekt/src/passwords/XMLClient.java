package passwords;

import java.io.File;
import java.util.HashMap;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class XMLClient {
    private NodeList nList;
    private Document doc;

    private void getElementsByName(String name, String file) throws Exception {
        File fXmlFile = new File(file);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        nList = doc.getElementsByTagName(name);
    }

    public HashMap<String, String> loadPasswordsFromXML() {
        HashMap<String, String> passwords = new HashMap<>();
        try {
            getElementsByName("user", "passwords.xml");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    String login = eElement.getElementsByTagName("login")
                            .item(0).getTextContent();
                    String password = eElement.getElementsByTagName("password")
                            .item(0).getTextContent();
                    passwords.put(login, password);

                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "brak zapisanych hasel",
                    "XML ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return passwords;
    }

    public void saveUser(String name, String password) throws Exception {
        getElementsByName("saved", "passwords.xml");
        Text loginText = doc.createTextNode(name);
        Element loginEl = doc.createElement("login");
        loginEl.appendChild(loginText);
        Text passwordTexy = doc.createTextNode(password);
        Element passwordEl = doc.createElement("password");
        passwordEl.appendChild(passwordTexy);
        Element user = doc.createElement("user");
        user.appendChild(loginEl);
        user.appendChild(passwordEl);

        nList.item(0).appendChild(user);
        TransformerFactory transformerFactory = TransformerFactory
                .newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(".\\passwords.xml"));
        transformer.transform(source, result);
        JOptionPane.showMessageDialog(null, "zapisano haslo", "",
                JOptionPane.OK_OPTION);
    }
}
