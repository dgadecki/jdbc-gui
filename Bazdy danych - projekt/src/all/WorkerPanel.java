package all;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import cars.MostWantedCars;

public class WorkerPanel {

    private JFrame frame;

    public WorkerPanel() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 1285, 632);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton btnDodajSamochd = new JButton("Dodaj samoch\u00F3d");
        btnDodajSamochd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });

        JButton btnNajpopularniejszeSamochody = new JButton(
                "Najpopularniejsze \r\nsamochody");
        btnNajpopularniejszeSamochody.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        try {
                            MostWantedCars frame = new MostWantedCars();
                            frame.setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
        groupLayout.setHorizontalGroup(groupLayout
                .createParallelGroup(Alignment.TRAILING)
                .addGroup(
                        groupLayout
                                .createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnDodajSamochd,
                                        GroupLayout.DEFAULT_SIZE, 1025,
                                        Short.MAX_VALUE)
                                .addGap(18)
                                .addComponent(btnNajpopularniejszeSamochody,
                                        GroupLayout.PREFERRED_SIZE, 206,
                                        GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                .addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 1269,
                        Short.MAX_VALUE));
        groupLayout
                .setVerticalGroup(groupLayout
                        .createParallelGroup(Alignment.LEADING)
                        .addGroup(
                                groupLayout
                                        .createSequentialGroup()
                                        .addComponent(tabbedPane,
                                                GroupLayout.PREFERRED_SIZE, 24,
                                                GroupLayout.PREFERRED_SIZE)
                                        .addGap(92)
                                        .addGroup(
                                                groupLayout
                                                        .createParallelGroup(
                                                                Alignment.BASELINE)
                                                        .addComponent(
                                                                btnDodajSamochd,
                                                                GroupLayout.PREFERRED_SIZE,
                                                                50,
                                                                GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(
                                                                btnNajpopularniejszeSamochody,
                                                                GroupLayout.PREFERRED_SIZE,
                                                                50,
                                                                GroupLayout.PREFERRED_SIZE))
                                        .addContainerGap(427, Short.MAX_VALUE)));

        frame.getContentPane().setLayout(groupLayout);
    }

    public void showWindow() {
        frame.setVisible(true);
    }
}
