package all;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import cars.AllCars;
import cars.FindCarForClientPanel;
import cars.MostWantedCarsForClient;

public class ClientTabbedPane extends JFrame {

    private AllCars allCars = new AllCars();
    private MostWantedCarsForClient mostWantedCarsForClient = new MostWantedCarsForClient();
    private JTabbedPane tabbedPane = new JTabbedPane();
    private FindCarForClientPanel findCar = new FindCarForClientPanel();
    private InformacjeOFirmie info = new InformacjeOFirmie();
    private Komunikator komunikator = new Komunikator();

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ClientTabbedPane frame = new ClientTabbedPane();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public ClientTabbedPane() {
        tabbedPane.add("All cars", allCars.getJPanel());
        tabbedPane.add("Most wanted", mostWantedCarsForClient.getJPanel());
        tabbedPane.add("Search car", findCar);
        tabbedPane.add("Kontakt", info);
        tabbedPane.add("Komunikator", komunikator);
        getContentPane().add(tabbedPane);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(973, 436);
    }
}
