package all;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Komunikator extends JPanel {

    /**
     * Create the panel.
     */
    public Komunikator() {
        setLayout(null);

        JButton btnWyslijWiadomosc = new JButton("Wyslij Wiadomosc");
        btnWyslijWiadomosc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });
        btnWyslijWiadomosc.setBounds(604, 286, 258, 69);
        add(btnWyslijWiadomosc);

        JButton btnOdswiez = new JButton("Odswiez");
        btnOdswiez.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        btnOdswiez.setBounds(604, 37, 258, 69);
        add(btnOdswiez);

        JLabel lblWiadomosc = new JLabel("Nowa wiadomosc:");
        lblWiadomosc.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblWiadomosc.setBounds(25, 251, 172, 14);
        add(lblWiadomosc);

        JLabel lblRozmowa = new JLabel("Rozmowa:");
        lblRozmowa.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblRozmowa.setBounds(25, 12, 172, 14);
        add(lblRozmowa);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(25, 37, 544, 199);
        add(scrollPane);

        JTextArea txtrRozmowa = new JTextArea();
        txtrRozmowa.setEditable(false);
        scrollPane.setViewportView(txtrRozmowa);

        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(25, 276, 544, 92);
        add(scrollPane_1);

        JTextArea txtrWiadomosc = new JTextArea();
        scrollPane_1.setViewportView(txtrWiadomosc);

        JButton btnWyczysc = new JButton("Wyczysc");
        btnWyczysc.setBounds(604, 142, 258, 69);
        add(btnWyczysc);

    }
}
