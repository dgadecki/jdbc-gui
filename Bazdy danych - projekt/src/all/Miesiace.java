package all;

public enum Miesiace {
    STYCZEN(1), LUTY(2), MARZEC(3), KWIECIEN(4), MAJ(5), CZERWIEC(6), LIPIEC(7), SIERPIEN(
            8), WRZESIEN(9), PAZDZIERNIK(10), LISTOPAD(11), GRUDZIEN(12);

    public int id = 0;
    private static Miesiace[] miesiace = values();

    public static Miesiace getMesiac(int m) {
        while (m < 0) {
            m += 12;
        }
        m %= 12;
        return miesiace[m];
    }

    Miesiace(int i) {
        id = i;
    }

    public int getOrder() {
        return id;
    }
}
