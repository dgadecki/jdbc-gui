package all;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import klient.Klient;
import klient.KlientDAO;
import klient.NowyKlientFrame;
import passwords.XMLClient;
import pracownik.PracownicyDAO;
import pracownik.Pracownik;

public class Login {

    public JFrame frame;
    private JTextField txtInsertLogin;
    private JTextField txtInsertPass;

    private String haslo;
    private String login;
    public static int idKlienta = 1;
    private XMLClient xml = new XMLClient();
    JRadioButton rdbtnZapamietajHaslo = new JRadioButton("Zapamietaj haslo");

    public Login() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setSize(409, 381);
        frame.setTitle("Logowanie");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lblLogin = new JLabel("Login:");
        lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 15));

        JLabel lblPass = new JLabel("Pass:");
        lblPass.setFont(new Font("Tahoma", Font.PLAIN, 15));

        JButton btnLogin = new JButton("LOGIN");
        btnLogin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                login = txtInsertLogin.getText();
                haslo = txtInsertPass.getText();

                if (login.isEmpty())
                    JOptionPane.showMessageDialog(null, "Login is empty!");
                else {
                    int odpowiedz = sprawdzLogowanie(login, haslo);
                    if (odpowiedz == 1) {
                        WorkerTabbedPane wp = new WorkerTabbedPane();
                        wp.show();
                        frame.dispose();
                    }
                    else if (odpowiedz == 2) {
                        ClientTabbedPane cp = new ClientTabbedPane();
                        cp.show();
                        frame.dispose();
                    }
                    else
                        JOptionPane.showMessageDialog(null, "Blad logowania");

                }
            }
        });
        btnLogin.setFont(new Font("Tahoma", Font.BOLD, 15));

        txtInsertLogin = new JTextField();
        txtInsertLogin.setFont(new Font("Tahoma", Font.PLAIN, 12));
        txtInsertLogin.setColumns(10);

        txtInsertPass = new JPasswordField();
        txtInsertPass.setFont(new Font("Tahoma", Font.PLAIN, 12));
        txtInsertPass.setColumns(10);

        JLabel lblWypoyczalniaSamochodw = new JLabel(
                "WYPO\u017BYCZALNIA SAMOCHOD\u00D3W");
        lblWypoyczalniaSamochodw.setFont(new Font("Tahoma", Font.BOLD, 20));

        JButton btnZarejestruj = new JButton("Zarejestruj");
        btnZarejestruj.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                NowyKlientFrame nowyKlientFrame = new NowyKlientFrame();
                nowyKlientFrame.frame.show();
            }
        });

        GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
        groupLayout
                .setHorizontalGroup(groupLayout
                        .createParallelGroup(Alignment.TRAILING)
                        .addGroup(
                                groupLayout
                                        .createSequentialGroup()
                                        .addGap(30)
                                        .addGroup(
                                                groupLayout
                                                        .createParallelGroup(
                                                                Alignment.TRAILING)
                                                        .addGroup(
                                                                groupLayout
                                                                        .createSequentialGroup()
                                                                        .addGroup(
                                                                                groupLayout
                                                                                        .createParallelGroup(
                                                                                                Alignment.TRAILING)
                                                                                        .addGroup(
                                                                                                groupLayout
                                                                                                        .createSequentialGroup()
                                                                                                        .addGap(59)
                                                                                                        .addGroup(
                                                                                                                groupLayout
                                                                                                                        .createParallelGroup(
                                                                                                                                Alignment.LEADING)
                                                                                                                        .addComponent(
                                                                                                                                lblPass)
                                                                                                                        .addComponent(
                                                                                                                                lblLogin))
                                                                                                        .addGap(18)
                                                                                                        .addGroup(
                                                                                                                groupLayout
                                                                                                                        .createParallelGroup(
                                                                                                                                Alignment.LEADING)
                                                                                                                        .addComponent(
                                                                                                                                txtInsertPass,
                                                                                                                                GroupLayout.DEFAULT_SIZE,
                                                                                                                                121,
                                                                                                                                Short.MAX_VALUE)
                                                                                                                        .addComponent(
                                                                                                                                txtInsertLogin,
                                                                                                                                GroupLayout.DEFAULT_SIZE,
                                                                                                                                121,
                                                                                                                                Short.MAX_VALUE)))
                                                                                        .addGroup(
                                                                                                groupLayout
                                                                                                        .createParallelGroup(
                                                                                                                Alignment.LEADING,
                                                                                                                false)
                                                                                                        .addComponent(
                                                                                                                btnLogin,
                                                                                                                GroupLayout.DEFAULT_SIZE,
                                                                                                                129,
                                                                                                                Short.MAX_VALUE)
                                                                                                        .addComponent(
                                                                                                                rdbtnZapamietajHaslo,
                                                                                                                GroupLayout.DEFAULT_SIZE,
                                                                                                                GroupLayout.DEFAULT_SIZE,
                                                                                                                Short.MAX_VALUE)
                                                                                                        .addComponent(
                                                                                                                btnZarejestruj,
                                                                                                                GroupLayout.DEFAULT_SIZE,
                                                                                                                GroupLayout.DEFAULT_SIZE,
                                                                                                                Short.MAX_VALUE)))
                                                                        .addGap(127))
                                                        .addGroup(
                                                                groupLayout
                                                                        .createSequentialGroup()
                                                                        .addComponent(
                                                                                lblWypoyczalniaSamochodw,
                                                                                GroupLayout.PREFERRED_SIZE,
                                                                                353,
                                                                                GroupLayout.PREFERRED_SIZE)
                                                                        .addContainerGap()))));
        groupLayout
                .setVerticalGroup(groupLayout
                        .createParallelGroup(Alignment.LEADING)
                        .addGroup(
                                groupLayout
                                        .createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(lblWypoyczalniaSamochodw,
                                                GroupLayout.PREFERRED_SIZE, 42,
                                                GroupLayout.PREFERRED_SIZE)
                                        .addGap(46)
                                        .addGroup(
                                                groupLayout
                                                        .createParallelGroup(
                                                                Alignment.BASELINE)
                                                        .addComponent(lblLogin)
                                                        .addComponent(
                                                                txtInsertLogin,
                                                                GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE,
                                                                GroupLayout.PREFERRED_SIZE))
                                        .addGap(18)
                                        .addGroup(
                                                groupLayout
                                                        .createParallelGroup(
                                                                Alignment.BASELINE)
                                                        .addComponent(lblPass)
                                                        .addComponent(
                                                                txtInsertPass,
                                                                GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE,
                                                                GroupLayout.PREFERRED_SIZE))
                                        .addGap(18)
                                        .addComponent(btnLogin,
                                                GroupLayout.PREFERRED_SIZE, 42,
                                                GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(
                                                ComponentPlacement.UNRELATED)
                                        .addComponent(rdbtnZapamietajHaslo)
                                        .addPreferredGap(
                                                ComponentPlacement.UNRELATED)
                                        .addComponent(btnZarejestruj)
                                        .addContainerGap(63, Short.MAX_VALUE)));
        frame.getContentPane().setLayout(groupLayout);
    }

    public int sprawdzLogowanie(String login, String haslo) {

        List<Pracownik> pracownicy = new ArrayList<>();
        List<Klient> klienci = new ArrayList<>();
        HashMap<String, String> savedPasswords = xml.loadPasswordsFromXML();
        if (haslo.isEmpty()) {
            haslo = savedPasswords.get(login);
        }
        else {
            haslo = hashMD5(haslo);
        }

        try {
            pracownicy = new PracownicyDAO().getAllPracownicy();
            klienci = new KlientDAO().getAllKlienci();

            for (Pracownik p : pracownicy) {
                if (login.equals(p.getLogin()) && haslo.equals(p.getHaslo())) {
                    if (!savedPasswords.containsKey(login)
                            && rdbtnZapamietajHaslo.isSelected()) {
                        try {
                            xml.saveUser(login, haslo);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null,
                                    "nie zapisano hasla", "XML ERROR",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    return 1;
                }
            }

            for (Klient k : klienci) {
                if (login.equals(k.getLogin()) && haslo.equals(k.getHaslo())) {
                    idKlienta = k.getId();
                    if (!savedPasswords.containsKey(login)
                            && rdbtnZapamietajHaslo.isSelected()) {
                        try {
                            xml.saveUser(login, haslo);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null,
                                    "nie zapisano hasla", "XML ERROR",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    return 2;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

        return 0;
    }

    public static String hashMD5(String toHash) {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            // Add password bytes to digest
            md.update(toHash.getBytes());
            // Get the hash's bytes
            byte[] bytes = md.digest();
            // This bytes[] has bytes in decimal format;
            // Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16)
                        .substring(1));
            }
            // Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

}
