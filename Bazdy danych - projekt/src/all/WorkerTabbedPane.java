package all;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import pracownik.NajlepsiPracownicy;
import przeglad.PrzegladyPanel;
import statystyki.PrzychodChartPanel;
import statystyki.PrzychodyPanel;
import ubezpieczenie.UbezpieczeniaPanel;
import cars.FindCarPanel;
import cars.MostWantedCars;

public class WorkerTabbedPane extends JFrame {

    private AddNewCarPanel addCarPanel = new AddNewCarPanel();

    private MostWantedCars most = new MostWantedCars();

    private JTabbedPane tabbedPane = new JTabbedPane();

    private FindCarPanel findCarPanel = new FindCarPanel();

    private NajlepsiPracownicy najlepsiPracownicy = new NajlepsiPracownicy();

    private PrzegladyPanel przegladyPanel = new PrzegladyPanel();

    private UbezpieczeniaPanel ubezpieczeniaPanel = new UbezpieczeniaPanel();

    private PrzychodyPanel przychodyPanel = new PrzychodyPanel();
    private PrzychodChartPanel przychodyChartPanel = new PrzychodChartPanel();
    private Komunikator komunikatorPracownik = new Komunikator();

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    WorkerTabbedPane frame = new WorkerTabbedPane();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public WorkerTabbedPane() {
        setTitle("Panel pracownika");
        tabbedPane.add("Most wanted", most.getJPanel());
        addCarPanel.btnDodaj.setVisible(true);
        addCarPanel.btnUpdate.setVisible(false);
        tabbedPane.add("Add car", addCarPanel);
        tabbedPane.add("Search car", findCarPanel);
        tabbedPane.add("Najlepsi pracownicy", najlepsiPracownicy);
        tabbedPane.add("Przeglady", przegladyPanel);
        tabbedPane.add("Ubezpieczenia", ubezpieczeniaPanel);
        tabbedPane.add("Statystyki samochodu", przychodyPanel);
        tabbedPane.add("Przychod", przychodyChartPanel);
        tabbedPane.add("Komunikator", komunikatorPracownik);
        getContentPane().add(tabbedPane);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(973, 436);
    }
}
