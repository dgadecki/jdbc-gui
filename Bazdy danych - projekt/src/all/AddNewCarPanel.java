package all;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import klasa.KlasaSamochodu;
import klasa.KlasaSamochoduDAO;
import stan.StanSamochodu;
import stan.StanSamochoduDAO;
import wypozyczalnia.Wypozyczalnia;
import wypozyczalnia.WypozyczalnieDAO;
import cars.Samochod;
import cars.SamochodyDAO;

public class AddNewCarPanel extends JPanel {

    private JLabel lblMarka = new JLabel("Marka");
    private JLabel lblModel = new JLabel("Model");
    private JLabel lblKlasa = new JLabel("Klasa");
    private JLabel lblStanSamochodu = new JLabel("Stan samochodu");
    private JLabel lblWypoyczalnia = new JLabel("Wypo\u017Cyczalnia");
    private JLabel lblMoc = new JLabel("Moc");
    private JLabel lblNrRejestracyjny = new JLabel("Nr rejestracyjny");
    private JLabel lblRokProdukcji = new JLabel("Rok produkcji");
    private JLabel lblRodzajSilnika = new JLabel("Rodzaj silnika");
    private JTextField txtMarka = new JTextField();
    private JTextField txtModel = new JTextField();
    private JTextField txtNrRejestracyjny = new JTextField();
    private JSpinner spRokProdukcji = new JSpinner(new SpinnerNumberModel(2014,
            1, Integer.MAX_VALUE, 1));
    private JSpinner spMoc = new JSpinner(new SpinnerNumberModel(1, 1,
            Integer.MAX_VALUE, 1));

    private JComboBox cbWypozyczalnia = new JComboBox();
    private JComboBox cbStanSamochodu = new JComboBox();
    private JComboBox cbKlasa = new JComboBox();
    private JComboBox cbRodzajSilnika = new JComboBox();
    public JButton btnUpdate = new JButton("Update");
    public JButton btnDodaj = new JButton("Dodaj");
    public Samochod actualCar;

    /**
     * Create the panel.
     */
    public AddNewCarPanel() {
        txtNrRejestracyjny.setBounds(187, 81, 86, 20);
        txtNrRejestracyjny.setColumns(10);
        txtModel.setBounds(187, 33, 86, 20);
        txtModel.setColumns(10);
        setLayout(null);

        lblMarka.setBounds(40, 11, 46, 14);
        add(lblMarka);

        lblModel.setBounds(40, 36, 46, 14);
        add(lblModel);

        lblRokProdukcji.setBounds(40, 59, 81, 14);
        add(lblRokProdukcji);

        lblNrRejestracyjny.setBounds(40, 84, 101, 14);
        add(lblNrRejestracyjny);

        lblRodzajSilnika.setBounds(40, 109, 81, 14);
        add(lblRodzajSilnika);

        lblMoc.setBounds(40, 134, 46, 14);
        add(lblMoc);

        lblKlasa.setBounds(40, 159, 46, 14);
        add(lblKlasa);

        lblStanSamochodu.setBounds(40, 184, 115, 14);
        add(lblStanSamochodu);

        lblWypoyczalnia.setBounds(40, 209, 101, 14);
        add(lblWypoyczalnia);

        txtMarka.setBounds(187, 8, 86, 20);
        add(txtMarka);
        txtMarka.setColumns(10);

        add(txtModel);

        add(txtNrRejestracyjny);

        spRokProdukcji.setBounds(187, 56, 86, 20);

        add(spRokProdukcji);

        spMoc.setBounds(187, 131, 86, 20);
        add(spMoc);

        cbWypozyczalnia.setBounds(187, 206, 86, 20);
        List<Wypozyczalnia> wyp = new ArrayList<Wypozyczalnia>();
        try {
            wyp = new WypozyczalnieDAO().getAllWypozyczalnie();
        } catch (Exception e) {

        }
        for (Wypozyczalnia w : wyp) {
            cbWypozyczalnia.addItem(w);
        }

        add(cbWypozyczalnia);

        cbStanSamochodu.setBounds(187, 181, 86, 20);
        List<StanSamochodu> st = new ArrayList<StanSamochodu>();
        try {
            st = new StanSamochoduDAO().getAllStanSamochodu();
        } catch (Exception e) {

        }
        for (StanSamochodu s : st) {
            cbStanSamochodu.addItem(s);
        }
        add(cbStanSamochodu);

        cbKlasa.setBounds(187, 156, 86, 20);
        List<KlasaSamochodu> kl = new ArrayList<KlasaSamochodu>();
        try {
            kl = new KlasaSamochoduDAO().getAllKlasySamochodu();
        } catch (Exception e) {

        }
        for (KlasaSamochodu k : kl) {
            cbKlasa.addItem(k);
        }

        add(cbKlasa);

        cbRodzajSilnika.setBounds(187, 106, 86, 20);
        cbRodzajSilnika.addItem("Benzyna");
        cbRodzajSilnika.addItem("Diesel");
        add(cbRodzajSilnika);
        btnDodaj.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int decide = JOptionPane.showConfirmDialog(AddNewCarPanel.this,
                        "Czy na pewno chcesz dodac samochod?");
                if (decide == 0) {
                    addCar();
                    JOptionPane.showMessageDialog(AddNewCarPanel.this,
                            "Pomyslnie dodano samochod");
                }
            }
        });

        btnDodaj.setBounds(98, 234, 115, 49);
        add(btnDodaj);
        btnUpdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int decide = JOptionPane.showConfirmDialog(AddNewCarPanel.this,
                        "Czy na pewno chcesz zaktualizowac samochod?");
                if (decide == 0) {
                    updateCar(actualCar);
                    JOptionPane.showMessageDialog(AddNewCarPanel.this,
                            "Pomyslnie zaktualizowano samochod");
                }
            }
        });
        btnUpdate.setBounds(98, 234, 115, 49);

        add(btnUpdate);
    }

    public void insertData(Samochod car) {
        txtMarka.setText(car.getmarka());
        txtModel.setText(car.getmodel());
        txtNrRejestracyjny.setText(car.getnrRejestracyjny());
        spRokProdukcji.setValue(car.getrokProdukcji());
        cbRodzajSilnika.setSelectedItem(car.getrodzajSilnika());
        spMoc.setValue(car.getmocSilnika());
        cbKlasa.setSelectedItem(car.getKlasaSamochoduObj());
        cbStanSamochodu.setSelectedItem(car.getStanSamochoduObj());
        cbWypozyczalnia.setSelectedItem(car.getWypozyczalniaObj());
    }

    public void updateCar(Samochod car) {
        Samochod samochod = new Samochod(car.getId(), txtMarka.getText(),
                txtModel.getText(), txtNrRejestracyjny.getText(),
                (int) spMoc.getValue(), (int) spRokProdukcji.getValue(),
                (String) cbRodzajSilnika.getSelectedItem(), "", "", "",
                ((StanSamochodu) cbStanSamochodu.getSelectedItem()).getId(),
                ((Wypozyczalnia) cbWypozyczalnia.getSelectedItem()).getId(),
                ((KlasaSamochodu) cbKlasa.getSelectedItem()).getId());
        try {
            SamochodyDAO samochodyDAO = new SamochodyDAO();
            samochodyDAO.updateSamochod(samochod);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public void addCar() {
        Samochod samochod = new Samochod(-1, txtMarka.getText(),
                txtModel.getText(), txtNrRejestracyjny.getText(),
                (int) spMoc.getValue(), (int) spRokProdukcji.getValue(),
                (String) cbRodzajSilnika.getSelectedItem(), "", "", "",
                ((StanSamochodu) cbStanSamochodu.getSelectedItem()).getId(),
                ((Wypozyczalnia) cbWypozyczalnia.getSelectedItem()).getId(),
                ((KlasaSamochodu) cbKlasa.getSelectedItem()).getId());
        try {
            SamochodyDAO samochodyDAO = new SamochodyDAO();
            samochodyDAO.addSamochod(samochod);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
