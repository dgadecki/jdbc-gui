package statystyki;

import cars.Samochod;

public class Przychod {

	private int przychod;
	private int dni;
	private int iloscWypozyczen;
	private Samochod samochod;
	
	public Przychod(int przychod, int dni, int iloscWypozyczen, String marka, String model,  int idSamochodu, String nrRejestracyjny){
		this.przychod = przychod;
		this.dni = dni;
		this.iloscWypozyczen = iloscWypozyczen;
		this.samochod = new Samochod(idSamochodu, model, marka, nrRejestracyjny);
	}
	
	public int getPrzychod(){
		return przychod;
	}
	
	public int getDni(){
		return dni;
	}
	
	public int getIloscWypozyczen(){
		return iloscWypozyczen;
	}
	
	public Samochod getSamochod(){
		return samochod;
	}
}

