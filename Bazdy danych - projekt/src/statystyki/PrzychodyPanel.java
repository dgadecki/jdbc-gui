package statystyki;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;

import statystyki.Przychod;
import statystyki.PrzychodDAO;
import statystyki.PrzychodTableModel;

public class PrzychodyPanel extends JPanel {
    JLabel lblMonths = new JLabel("Ilosc ostatnich miesiecy:");
    private JTable table;
    JButton btnSzukaj = new JButton("Generuj");
    PrzychodDAO przychodDAO;
    JScrollPane scrollPane = new JScrollPane();
    List<Przychod> przychody;
    private final JSpinner spinner = new JSpinner();

    /**
     * Create the panel.
     */
    public PrzychodyPanel() {
        try {
        	przychodDAO = new PrzychodDAO();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        setLayout(null);

        lblMonths.setBounds(31, 11, 171, 14);
        add(lblMonths);

        spinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
        spinner.setBounds(212, 8, 54, 20);
        
        scrollPane.setBounds(10, 41, 767, 306);
        add(scrollPane);

        table = new JTable();
        scrollPane.setViewportView(table);
        btnSzukaj.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                try {
                	int nMonths = (int) spinner.getValue();
                    przychody = przychodDAO.getPrzychodyMonths(nMonths);
                    PrzychodTableModel przychodyTableModel = new PrzychodTableModel(przychody);
                    table.setModel(przychodyTableModel);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(PrzychodyPanel.this, "Error: "
                            + e, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        btnSzukaj.setBounds(305, 7, 89, 23);
        add(btnSzukaj);
        
        
        add(spinner);

    }
}
