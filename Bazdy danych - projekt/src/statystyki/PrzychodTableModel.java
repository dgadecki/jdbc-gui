package statystyki;

import java.util.List;

import javax.swing.table.AbstractTableModel;

public class PrzychodTableModel extends AbstractTableModel {

	private static final int PRZYCHOD = 0;
	private static final int DNI = 1;
	private static final int WYPOZYCZENIA = 2;
	private static final int MARKA = 3;
	private static final int MODEL = 4;
	private static final int ID_SAMOCHODU = 5;
	private static final int NR_REJESTRACYJNY = 6;
	
	
	private String[] columnNames = {"Przychod", "Ilosc dni", "Wypozyczenia", "Marka", "Model", "ID", "Nr rej."};
	
	private List<Przychod> przychody;
	
	public PrzychodTableModel(List<Przychod> p){
		przychody = p;
	}
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return przychody.size();
	}

	@Override
	public String getColumnName(int col){
		return columnNames[col];
	}
	
	@Override
	public Object getValueAt(int row, int col) {
		Przychod tmpPrzychod = przychody.get(row);
		switch(col){
		case PRZYCHOD:
			return tmpPrzychod.getPrzychod();
		case DNI:
			return tmpPrzychod.getDni();
		case WYPOZYCZENIA:
			return tmpPrzychod.getIloscWypozyczen();
		case MARKA:
			return tmpPrzychod.getSamochod().getmarka();
		case MODEL:
			return tmpPrzychod.getSamochod().getmodel();
		case ID_SAMOCHODU:
			return tmpPrzychod.getSamochod().getId();
		case NR_REJESTRACYJNY:
			return tmpPrzychod.getSamochod().getnrRejestracyjny();
		default:
			return tmpPrzychod.getPrzychod();
		}
	}
	
	@Override
	public Class getColumnClass(int c){
		return getValueAt(0, c).getClass();
	}

	
}
