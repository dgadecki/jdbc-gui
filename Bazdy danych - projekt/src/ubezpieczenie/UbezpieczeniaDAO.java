package ubezpieczenie;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class UbezpieczeniaDAO {
    private Connection myConn;

    public UbezpieczeniaDAO() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileInputStream("bazydanych.properties"));

        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String dburl = properties.getProperty("dburl");

        myConn = DriverManager.getConnection(dburl, user, password);
    }

    public List<Ubezpieczenie> getAllUbezpieczenia() throws Exception {
        List<Ubezpieczenie> ubezpieczenia = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt.executeQuery("select poczatek, koniec, nazwa_ubezpieczyciela, id_samochodu, marka, model_samochodu, nr_rejestracyjny FROM ubezpieczenia U inner join samochody S on U.Samochody_id_samochodu= S.id_samochodu");
            while (myRs.next()) {
                Ubezpieczenie ubezpieczenie = convertRowToUbezpieczenie(myRs);
                ubezpieczenia.add(ubezpieczenie);
            }
            return ubezpieczenia;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }
    
    public List<Ubezpieczenie> getUbezpieczeniaApToMonths(int months) throws Exception {
        List<Ubezpieczenie> ubezpieczenia = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt.executeQuery("select poczatek, koniec, nazwa_ubezpieczyciela, marka, model_samochodu, nr_rejestracyjny, id_samochodu"
            		+ " FROM ubezpieczenia U inner join samochody S on U.Samochody_id_samochodu= S.id_samochodu"
            		+ " WHERE U.koniec<adddate(current_date, interval " +months+ " month)");
            while (myRs.next()) {
                Ubezpieczenie ubezpieczenie = convertRowToUbezpieczenie(myRs);
                ubezpieczenia.add(ubezpieczenie);
            }
            return ubezpieczenia;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    private Ubezpieczenie convertRowToUbezpieczenie(ResultSet myRs)
            throws SQLException {
        Ubezpieczenie ubezpieczenia = new Ubezpieczenie(
                myRs.getString("poczatek"), myRs.getString("koniec"), 
                myRs.getString("nazwa_ubezpieczyciela"), myRs.getInt("id_samochodu"), myRs.getString("marka"),
                myRs.getString("model_samochodu"), myRs.getString("nr_rejestracyjny"));
        return ubezpieczenia;
    }
}
