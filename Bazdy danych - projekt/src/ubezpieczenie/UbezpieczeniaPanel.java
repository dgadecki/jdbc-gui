package ubezpieczenie;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;

public class UbezpieczeniaPanel extends JPanel {
    JLabel lblMonths = new JLabel("Ilosc miesiecy do konca ubezpieczenia");
    private JTable table;
    JButton btnSzukaj = new JButton("Szukaj");
    UbezpieczeniaDAO ubezpieczeniaDAO;
    JScrollPane scrollPane = new JScrollPane();
    List<Ubezpieczenie> ubezpieczenia;
    private final JSpinner spinner = new JSpinner();

    /**
     * Create the panel.
     */
    public UbezpieczeniaPanel() {
        try {
            ubezpieczeniaDAO = new UbezpieczeniaDAO();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        setLayout(null);

        lblMonths.setBounds(10, 11, 221, 14);
        add(lblMonths);

        spinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1),
                null, new Integer(1)));
        spinner.setBounds(241, 8, 54, 20);

        scrollPane.setBounds(10, 41, 767, 306);
        add(scrollPane);

        table = new JTable();
        scrollPane.setViewportView(table);
        btnSzukaj.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                try {
                    int nMonths = (int) spinner.getValue();
                    ubezpieczenia = ubezpieczeniaDAO
                            .getUbezpieczeniaApToMonths(nMonths);
                    UbezpieczeniaTableModel ubezpieczeniaTableModel = new UbezpieczeniaTableModel(
                            ubezpieczenia);
                    table.setModel(ubezpieczeniaTableModel);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(UbezpieczeniaPanel.this,
                            "Error: " + e, "Error", JOptionPane.ERROR_MESSAGE);

                }
            }
        });

        btnSzukaj.setBounds(305, 7, 89, 23);
        add(btnSzukaj);

        add(spinner);

        JButton btnShowAll = new JButton("Pokaz wszystkie");
        btnShowAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    ubezpieczenia = ubezpieczeniaDAO.getAllUbezpieczenia();
                    UbezpieczeniaTableModel ubezpieczeniaTableModel = new UbezpieczeniaTableModel(
                            ubezpieczenia);
                    table.setModel(ubezpieczeniaTableModel);
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(UbezpieczeniaPanel.this,
                            "Error: " + e1, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        btnShowAll.setBounds(420, 7, 135, 23);
        add(btnShowAll);

    }
}
