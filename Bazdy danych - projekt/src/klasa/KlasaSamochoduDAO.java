package klasa;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class KlasaSamochoduDAO {
    private Connection myConn;

    public KlasaSamochoduDAO() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileInputStream("bazydanych.properties"));

        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String dburl = properties.getProperty("dburl");

        myConn = DriverManager.getConnection(dburl, user, password);
    }

    public List<KlasaSamochodu> getAllKlasySamochodu() throws Exception {
        List<KlasaSamochodu> klasySamochodu = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt.executeQuery("select * from Klasy_samochodow");
            while (myRs.next()) {
                KlasaSamochodu klasaSamochodu = convertRowToKlasaSamochodu(myRs);
                klasySamochodu.add(klasaSamochodu);
            }
            return klasySamochodu;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    private KlasaSamochodu convertRowToKlasaSamochodu(ResultSet myRs)
            throws SQLException {
        KlasaSamochodu klasaSamochodu = new KlasaSamochodu(
                myRs.getInt("id_klasy"), myRs.getString("nazwa"),
                myRs.getDouble("cena_za_dzien"));
        return klasaSamochodu;
    }
}