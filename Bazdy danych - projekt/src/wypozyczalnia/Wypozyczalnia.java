package wypozyczalnia;

public class Wypozyczalnia {
    private int id = -1;
    private String miasto = "";

    public Wypozyczalnia(int id, String miasto) {
        this.id = id;
        this.miasto = miasto;
    }

    public int getId() {
        return id;
    }

    public String getMiasto() {
        return miasto;
    }

    @Override
    public String toString() {
        return miasto;
    }

    @Override
    public boolean equals(Object o) {
        return getId() == ((Wypozyczalnia) o).getId();
    }
}
