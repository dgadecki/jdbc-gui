package wypozyczalnia;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class WypozyczalnieDAO {
    private Connection myConn;

    public WypozyczalnieDAO() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileInputStream("bazydanych.properties"));

        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String dburl = properties.getProperty("dburl");

        myConn = DriverManager.getConnection(dburl, user, password);
    }

    public List<Wypozyczalnia> getAllWypozyczalnie() throws Exception {
        List<Wypozyczalnia> wypozyczalnie = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt.executeQuery("select * from Wypozyczalnie");
            while (myRs.next()) {
                Wypozyczalnia wypozyczalnia = convertRowToWypozyczalnia(myRs);
                wypozyczalnie.add(wypozyczalnia);
            }
            return wypozyczalnie;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    private Wypozyczalnia convertRowToWypozyczalnia(ResultSet myRs)
            throws SQLException {
        Wypozyczalnia wypozyczalnia = new Wypozyczalnia(
                myRs.getInt("id_wypozyczalni"), myRs.getString("miasto"));
        return wypozyczalnia;
    }

}
