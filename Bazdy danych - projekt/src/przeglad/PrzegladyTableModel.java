package przeglad;

import javax.swing.table.AbstractTableModel;

import java.io.ObjectInputStream.GetField;
import java.util.List;

public class PrzegladyTableModel extends AbstractTableModel {

	private static final int DATA_PRZEGLADU = 0;
	private static final int KONIEC_PRZEGLADU = 1;
	private static final int MARKA = 2;
	private static final int MODEL = 3;
	private static final int NR_REJESTRACYJNY = 4;
	
	private String[] columnNames = {"Ostatni przeglad", "Kolejny przeglad", "Marka",
						"Model", "Nr rej."};
	
	private List<Przeglad> przeglady;
	
	public PrzegladyTableModel(List<Przeglad> p){
		przeglady = p;
	}
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return przeglady.size();
	}

	@Override
	public String getColumnName(int col){
		return columnNames[col];
	}
	
	@Override
	public Object getValueAt(int row, int col) {
		Przeglad tmpPrzeglad = przeglady.get(row);
		switch(col){
		case DATA_PRZEGLADU:
			return tmpPrzeglad.getDataPrzegladu();
		case KONIEC_PRZEGLADU:
			return tmpPrzeglad.getKoniecPrzegladu();
		case MARKA:
			return tmpPrzeglad.getSamochod().getmarka();
		case MODEL:
			return tmpPrzeglad.getSamochod().getmodel();
		case NR_REJESTRACYJNY:
			return tmpPrzeglad.getSamochod().getnrRejestracyjny();
		default:
			return tmpPrzeglad.getId();
		}
	}
	
	@Override
	public Class getColumnClass(int c){
		return getValueAt(0, c).getClass();
	}

	
}
