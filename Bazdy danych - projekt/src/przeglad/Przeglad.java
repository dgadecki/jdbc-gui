package przeglad;

import cars.Samochod;


public class Przeglad {
    private int id = -1;
    private String dataPrzegladu = "";
    private String koniecPrzegladu = "";
    private Samochod samochod;

    public Przeglad(int id, String dataPrzegladu, String koniecPrzegladu, 
    		int idSamochodu, String marka, String model, String nrRejestracyjny) {
        this.id = id;
        this.dataPrzegladu = dataPrzegladu;
        this.koniecPrzegladu = koniecPrzegladu;
        this.samochod = new Samochod(idSamochodu, marka, model, nrRejestracyjny);
    }

    public int getId() {
        return id;
    }

    public String getDataPrzegladu() {
        return dataPrzegladu;
    }
    
    public String getKoniecPrzegladu(){
    	return koniecPrzegladu;
    }
    
    public Samochod getSamochod(){
    	return samochod;
    }
    
    public void setDataPrzegladu(String dataPrzegladu){
    	this.dataPrzegladu = dataPrzegladu;
    }
    
    public void setKoniecPrzegladu(String koniecPrzegladu){
    	this.koniecPrzegladu = koniecPrzegladu;
    }

    @Override
    public String toString() {
        return dataPrzegladu + " - " + koniecPrzegladu;
    }

    @Override
    public boolean equals(Object o) {
        return getId() == ((Przeglad) o).getId();
    }
}
