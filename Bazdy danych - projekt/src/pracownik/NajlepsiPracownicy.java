package pracownik;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;

public class NajlepsiPracownicy extends JPanel {
    private JTable table;
    private PracownicyDAO pracownicyDAO;
    JSpinner spinnerMiesiace = new JSpinner();

    /**
     * Create the panel.
     */
    public NajlepsiPracownicy() {
        try {
            pracownicyDAO = new PracownicyDAO();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        setSize(973, 436);
        setLayout(null);

        JLabel lblLiczbaMiesicy = new JLabel("Liczba miesi\u0119cy:");
        lblLiczbaMiesicy.setBounds(28, 19, 95, 14);
        add(lblLiczbaMiesicy);

        spinnerMiesiace.setModel(new SpinnerNumberModel(new Integer(1),
                new Integer(1), null, new Integer(1)));
        spinnerMiesiace.setBounds(133, 16, 46, 20);
        add(spinnerMiesiace);

        JButton btnPokaz = new JButton("Pokaz");
        btnPokaz.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                List<Pracownik> prac = new ArrayList<Pracownik>();
                try {
                    prac = pracownicyDAO
                            .getNajlepsiPracownicy((int) spinnerMiesiace
                                    .getValue());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(NajlepsiPracownicy.this,
                            "Error: " + ex, "Error", JOptionPane.ERROR_MESSAGE);
                }
                table.setModel(new NajlepsiPracownicyTableModel(prac));
            }
        });
        btnPokaz.setBounds(211, 11, 95, 30);
        add(btnPokaz);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 85, 604, 207);
        add(scrollPane);

        table = new JTable();
        scrollPane.setViewportView(table);

    }
}
