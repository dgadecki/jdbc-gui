package pracownik;

public class Pracownik {
    private int id = -1;
    private String imie = "";
    private String nazwisko = "";
    private String login = "";
    private String haslo = "";
    private int przychod = 0;

    public Pracownik(int id, String imie, String nazwisko, String login,
            String haslo) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.login = login;
        this.haslo = haslo;
    }

    public Pracownik(int id, String imie, String nazwisko, int przychod) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.przychod = przychod;
    }

    @Override
    public String toString() {
        return imie + " " + nazwisko;
    }

    public int getId() {
        return id;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public String getLogin() {
        return login;
    }

    public String getHaslo() {
        return haslo;
    }

    public int getPrzychod() {
        return przychod;
    }
}
