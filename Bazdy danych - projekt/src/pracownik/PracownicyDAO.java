package pracownik;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PracownicyDAO {
    private Connection myConn;

    public PracownicyDAO() throws Exception {
        Properties properties = new Properties();
        properties.load(new FileInputStream("bazydanych.properties"));

        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String dburl = properties.getProperty("dburl");

        myConn = DriverManager.getConnection(dburl, user, password);
    }

    public List<Pracownik> getAllPracownicy() throws Exception {
        List<Pracownik> pracownicy = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt.executeQuery("select * from pracownicy");
            while (myRs.next()) {
                Pracownik pracownik = convertRowToPracownicy(myRs);
                pracownicy.add(pracownik);
            }
            return pracownicy;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    public List<Pracownik> getNajlepsiPracownicy(int months) throws Exception {
        List<Pracownik> pracownicy = new ArrayList<>();
        Statement myStmt = null;
        ResultSet myRs = null;
        months *= -1;
        try {
            myStmt = myConn.createStatement();
            myRs = myStmt
                    .executeQuery("select id_pracownika, imie, nazwisko, sum(oplata_koncowa)"
                            + " as przychod  from pracownicy P left outer join wypozyczenia "
                            + "W on P.id_pracownika = W.Pracownicy_id_pracownika where "
                            + "W.poczatek between adddate(current_date, interval "
                            + months
                            + " month) "
                            + " and current_date group by P.id_pracownika order by przychod DESC");
            while (myRs.next()) {
                Pracownik pracownik = convertRowToNajlepsiPracownicy(myRs);
                pracownicy.add(pracownik);
            }
            return pracownicy;
        } finally {
            myStmt.close();
            myRs.close();
        }
    }

    private Pracownik convertRowToPracownicy(ResultSet myRs)
            throws SQLException {
        Pracownik pracownicy = new Pracownik(myRs.getInt("id_pracownika"),
                myRs.getString("imie"), myRs.getString("nazwisko"),
                myRs.getString("login"), myRs.getString("haslo"));
        return pracownicy;
    }

    private Pracownik convertRowToNajlepsiPracownicy(ResultSet myRs)
            throws SQLException {
        Pracownik pracownicy = new Pracownik(myRs.getInt("id_pracownika"),
                myRs.getString("imie"), myRs.getString("nazwisko"),
                myRs.getInt("przychod"));
        return pracownicy;
    }
}
